Name:          ambience-yrgb-black
Version:       0.2.0
Release:       1
Summary:       Four colored black ambiences
Group:         System/Tools
Vendor:        eson
Distribution:  SailfishOS
Packager:      eson
URL:           https://openrepos.net/content/eson/ambience-yrgb-black

License:       GPL

%description
Four different colored ambiences based on black(ish) background images.

%files
%defattr(-,root,root,-)
/usr/share/ambience/*

%post
chmod 755 /usr/share/ambience/{name}
chmod 755 /usr/share/ambience/{name}/images
chmod 755 /usr/share/ambience/{name}/sounds
chmod 644 /usr/share/ambience/{name}/*.*
chmod 644 /usr/share/ambience/{name}/images/*.*
chmod 644 /usr/share/ambience/{name}/sounds/*.*
systemctl-user restart ambienced.service

%postun
if [ $1 = 0 ]; then
rm -rf /usr/share/ambience/{name}
systemctl-user restart ambienced.service
else
if [ $1 = 1 ]; then
echo "Upgrading"
systemctl-user restart ambienced.service
fi
fi

%changelog
* Sat Sep 23 2017 0.2
- Fixed not working im notification signal.
- Replaced YellowBlack image

* Wed Sep 20 2017 0.1
- Initial release.
